FROM python:3.9
WORKDIR ./virgo
COPY .. .
RUN apt-get update
RUN pip install pandas
ENTRYPOINT [ "python3", "solution/solution.py" ]