# DWH Coding Challenge

## Git URL
https://gitlab.com/g_adhitia/test-virgo

## Summary of My Solution
The thinking behind the solution that I implemented, was to create a historical table by creating a history table using the SDC type 2 technique. In each file, initially a new line is created based on the `id` and `timestamp`, if the type of DB operation is `update`, then the value for the other column will be taken according to the value of the previous row, then enter data from each file according to the contents of the field `set`.

To count the number of transactions, I created a flag to find which rows have changes in the credit_used and balance columns. Then the dataset is cut according to the flags that have been created.

## Run via Python
1. Go to root dir
1. Run ```python .\solution\solution.py```

## Run via Docker
1. Run docker desktop
1. Go to root dir
1. Create docker image ```docker build -f Dockerfile -t dwhcodingchallenge:v1 .```
1. Run docker image ```docker run dwhcodingchallenge:v1```