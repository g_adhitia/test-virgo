import glob
import json
import pandas as pd
import sys


def print_to_stdout(*a):
    print(*a, file=sys.stdout)


def create_new_row(data, df):
    df_temp = pd.DataFrame({'id': [data['id']], 'ts': [data['ts']]})
    df = pd.concat([df, df_temp], ignore_index=True)
    return df


def insert_data(data, df, flag):
    for key, value in data.items():
        if key == 'status' and flag == 'card': key = 'status_card'
        if key == 'status' and flag == 'saving': key = 'status_saving'
        df.iloc[-1, df.columns.get_loc(key)] = value
    return df


def get_data(file, df, flag='none'):
    with open(file, 'r') as f:
        data = json.loads(f.read())

    df = create_new_row(data, df)

    if data['op'] == 'c' and (flag == 'account' or flag == 'none'):
        df = insert_data(data['data'], df, flag)
    elif data['op'] == 'c' and (flag == 'card' or flag == 'saving'):
        df.iloc[-1, ~df.columns.isin(['id', 'ts'])] = df.iloc[-2, ~df.columns.isin(['id', 'ts'])]
        df = insert_data(data['data'], df, flag)
    else:
        df.iloc[-1, ~df.columns.isin(['id', 'ts'])] = df.iloc[-2, ~df.columns.isin(['id', 'ts'])]
        df = insert_data(data['set'], df, flag)
    return df


# Read Data
files_accounts = glob.glob('data/accounts/*')
files_cards = glob.glob('data/cards/*')
files_saving = glob.glob('data/savings_accounts/*')

files_accounts = sorted(files_accounts, reverse=False)
files_cards = sorted(files_cards, reverse=False)
files_saving = sorted(files_saving, reverse=False)

# Create Data Frame
df_account = pd.DataFrame(columns=[
    'id', 'ts', 'account_id', 'name', 'address', 'phone_number', 'email', 'savings_account_id', 'card_id'
])
df_cards = pd.DataFrame(columns=[
    'id', 'ts', 'card_id', 'card_number', 'credit_used', 'monthly_limit', 'status'
])
df_saving = pd.DataFrame(columns=[
    'id', 'ts', 'savings_account_id', 'balance', 'interest_rate_percent', 'status'
])
df_all = pd.DataFrame(columns=[
    'id', 'ts', 'account_id', 'name', 'address', 'phone_number', 'email', 'savings_account_id', 'card_id',
    'card_number', 'credit_used', 'monthly_limit', 'status_card', 'balance', 'interest_rate_percent', 'status_saving'
])

# Capture historical data each table
for file in files_accounts:
    df_account = get_data(file, df_account)
for file in files_cards:
    df_cards = get_data(file, df_cards)
for file in files_saving:
    df_saving = get_data(file, df_saving)

# Add flag
f1 = df_account[['ts']].reset_index(drop=True)
f1.loc[:, 'flag'] = 'account'
f2 = df_cards[['ts']].reset_index(drop=True)
f2.loc[:, 'flag'] = 'card'
f3 = df_saving[['ts']].reset_index(drop=True)
f3.loc[:, 'flag'] = 'saving'

# Get timestamp
df_temp_ts = pd.concat([f1, f2, f3]).sort_values(by=['ts']).reset_index(drop=True)

# Capture historical all data
for index, row in df_temp_ts.iterrows():
    if (row['flag'] == 'account'):
        file = 'data/accounts/' + str(row['ts']) + '.json'
    elif (row['flag'] == 'card'):
        file = 'data/cards/' + str(row['ts']) + '.json'
    elif (row['flag'] == 'saving'):
        file = 'data/savings_accounts/' + str(row['ts']) + '.json'
    df_all = get_data(file, df_all, row['flag'])

# Transaction
df_trans = df_all[['id', 'ts', 'account_id', 'savings_account_id', 'card_id', 'credit_used', 'balance']]
df_trans = df_trans.dropna(subset=['credit_used'])
df_trans = df_trans.dropna(subset=['balance'])

df_trans['match1'] = df_trans.credit_used == df_trans.credit_used.shift()
df_trans['match2'] = df_trans.balance == df_trans.balance.shift()

df_trans = df_trans.loc[
    ((df_trans['match1'] == False) | (df_trans['match2'] == False)) &
    ~((df_trans['match1'] == False) & (df_trans['match2'] == False))
    ].reset_index(drop=True)

df_trans_credit = df_trans.loc[(df_trans['match1'] == False)].reset_index(drop=True)
df_trans_balance = df_trans.loc[(df_trans['match2'] == False)].reset_index(drop=True)

df_trans = df_trans.drop(['match1', 'match2'], axis=1)
df_trans_credit = df_trans_credit.drop(['savings_account_id', 'balance', 'match1', 'match2'], axis=1)
df_trans_balance = df_trans_balance.drop(['card_id', 'credit_used', 'match1', 'match2'], axis=1)

# Show data
print_to_stdout("\n1. Visualize the complete historical table view of each tables in tabular format in stdout")
print_to_stdout("\n-----Historical Table Account-----")
print_to_stdout(df_account)

print_to_stdout("\n-----Historical Table Card-----")
print_to_stdout(df_cards)

print_to_stdout("\n-----Historical Table Saving-----")
print_to_stdout(df_saving)

print_to_stdout("\n2. Visualize the complete historical table view of the denormalized joined "
                "table in stdout by joining these three tables")

print_to_stdout("\n-----Complete Historical Table-----")
print_to_stdout(df_all)

print_to_stdout("\n3. From result from point no 2, discuss how many transactions has been made, "
                "when did each of them occur, and how much the value of each transaction?")

print_to_stdout('\nTransactions has been made = ' + str(df_trans.shape[0]))

print_to_stdout("\n-----Transactions Table-----")
print_to_stdout(df_trans)

print_to_stdout("\n-----Transactions Table Credit-----")
print_to_stdout(df_trans_credit)

print_to_stdout("\n-----Transactions Table Balance-----")
print_to_stdout(df_trans_balance)
